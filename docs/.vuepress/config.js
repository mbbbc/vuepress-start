module.exports = {
    // 显示在左上角的网页名称以及首页在浏览器标签显示的title名称
    title: 'Hello VuePress',
    // meta 中的描述文字，用于SEO
    description: 'Just playing around',
    // 注入到当前页面的 HTML <head> 中的标签
    head: [
        ['link', { rel: 'icon', href: '/logo.png' }],  //浏览器的标签栏的网页图标
    ],
    //配置vuepress-theme-reco主题，官网：https://vuepress-theme-reco.recoluan.com/
    theme: 'reco',
    //主题配置，对于不同的主题，选项可能会有不同
    themeConfig: {
        //指定博客风格首页布局
        type: 'blog',
        //设置首页右侧信息栏头像
        authorAvatar: '/logo.png',
        //在所有页面中启用自动生成子侧边栏，原 sidebar 仍然兼容
        subSidebar: 'auto',
        // 博客配置
        blogConfig: {
            category: {
                location: 2,     // 在导航栏菜单中所占的位置，默认2
                text: '分类' // 默认文案 “分类”
            },
            tag: {
                location: 3,     // 在导航栏菜单中所占的位置，默认3
                text: '标签'      // 默认文案 “标签”
            },
            socialLinks: [     // 信息栏展示社交信息
                { icon: 'reco-github', link: 'https://github.com/recoluan' },
                { icon: 'reco-npm', link: 'https://www.npmjs.com/~reco_luan' }
            ]
        },
        //添加友链
        friendLink: [
            {
              //友链标题
              title: 'vuepress-theme-reco',
              //友链描述
              desc: 'A simple and beautiful vuepress Blog & Doc theme.',
              //友链 LOGO（本地图片或网络图片）
              logo: "https://vuepress-theme-reco.recoluan.com/icon_vuepress_reco.png",
              //友链地址
              link: 'https://vuepress-theme-reco.recoluan.com'
            },
            {
              title: '午后南杂',
              desc: 'Enjoy when you can, and endure when you must.',
              email: 'recoluan@qq.com',
              link: 'https://www.recoluan.com'
            },
        ],
        //导航栏配置
        nav: [
            { text: '主页', link: '/' },
            { text: '指南', link: '/views/guide/markdown' },
            { text: 'External', link: 'https://google.com' },
        ],
        //侧边栏配置
        sidebar: {
            '/views/guide/': [
                {
                    //侧边栏标题
                    title: '指南',
                    //文档聚合成组
                    children: [
                        'markdown',
                        'link'
                    ]
                }
            ],
        },
        
    },
    //插件配置
    plugins: [
        ['@vuepress-reco/vuepress-plugin-back-to-top'],
        ['@vuepress-reco/pagation'],
        ['@vuepress/medium-zoom',{selector: '.theme-reco-content :not(a) > img'}],
        ["vuepress-plugin-nuggets-style-copy", {
            copyText: "复制代码",
            tip: {
                content: "复制成功"
            }
        }],
        ['@vuepress-reco/vuepress-plugin-bgm-player',{
            audios: [
                // 网络文件示例
                {
                    name: '我再没见过 像你一般的星空',
                    artist: 'Seto',
                    url: 'https://assets.smallsunnyfox.com/music/2.mp3',
                    cover: 'https://assets.smallsunnyfox.com/music/2.jpg'
                },
                {
                    name: '萤火之森',
                    artist: 'CMJ',
                    url: 'https://assets.smallsunnyfox.com/music/3.mp3',
                    cover: 'https://assets.smallsunnyfox.com/music/3.jpg'
                }
            ],
            position: {
                left: '5px',
                bottom: '10px',
                'z-index': '999999'
            },
            autoShrink: true,
            shrinkMode: 'float',//mini | float
        }],
    ]
}