---
title: VuePress对Markdown扩展
date: 2022-05-16 10:22
sidebar: 'auto'
categories: 
 - 'VuePress'
tags: 
 - 'VuePress'
 - 'Markdown'
---
:::tip
本文主要介绍VuePress对Markdown扩展，内容来自VuePress官网。
:::

![](./img/markdown.jpeg)
<!-- more -->


## Markdown 是什么？

:::tip
Markdown 是一种轻量级的标记语言，可用于在纯文本文档中添加格式化元素。Markdown 由 John Gruber 于 2004 年创建，如今已成为世界上最受欢迎的标记语言之一。
:::

更多：[Markdown 官方教程](https://markdown.com.cn/)

## 链接
### 内部链接

:::tip
站内的每一个文件夹下的 `README.md` 或者 `index.md` 文件都会被自动编译为 `index.html`，对应的链接将被视为 `/`。
:::

以如下的文件结构为例：

```
.
├─ README.md
├─ foo
│  ├─ README.md
│  ├─ one.md
│  └─ two.md
└─ bar
   ├─ README.md
   ├─ three.md
   └─ four.md
```

假设你现在在 `foo/one.md` 中

```markdown
[Home](/) <!-- 跳转到根部的 README.md -->
[foo](/foo/) <!-- 跳转到 foo 文件夹的 index.html -->
[foo heading](./#heading) <!-- 跳转到 foo/index.html 的特定标题位置 -->
[bar - three](../bar/three.md) <!-- 具体文件可以使用 .md 结尾（推荐） -->
[bar - four](../bar/four.html) <!-- 也可以用 .html -->
```



### 外部链接

外部的链接将会被自动地设置为 `target="_blank" rel="noopener noreferrer"`:

- [vuejs.org](https://vuejs.org/)
- [VuePress on GitHub](https://github.com/vuejs/vuepress)



## Front Matter

VuePress 提供了对 [YAML front matter ](https://jekyllrb.com/docs/frontmatter/)开箱即用的支持:

```yaml
---
title: Blogging Like a Hacker
lang: en-US
---
```

:::tip
在 `vuepress-theme-reco` 中，请摒弃一级标题，使用 `front-matter` 生成标题以及其他文章信息，正文从二级标题开始。
:::


## GitHub 风格的表格

**输入**

```
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
```

**输出**

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |



## Emoji

**输入**

```
:tada: :100:
```

**输出**

:tada: :100:

你可以在[这个列表](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/full.json)找到所有可用的 Emoji



## 目录

**输入**

```
[[toc]]
```

**输出**



[[toc]]



目录（Table of Contents）的渲染可以通过 [`markdown.toc`](https://www.vuepress.cn/config/#markdown-toc) 选项来配置。默认展示2、3级标题。

使用`vuepress-theme-reco`主题，目录结构在右侧边栏展示，更直观，这玩意就感觉没必要了:rofl:。​



## 自定义容器

**输入**

```
::: tip
这是一个提示
:::

::: warning
这是一个警告
:::

::: danger
这是一个危险警告
:::

::: details
这是一个详情块，在 IE / Edge 中不生效
:::
```

**输出**

::: tip
这是一个提示
:::

::: warning
这是一个警告
:::

::: danger
这是一个危险警告
:::

::: details
这是一个详情块，在 IE / Edge 中不生效
:::

你也可以自定义块中的标题：

```
::: danger STOP
危险区域，禁止通行
:::

::: details 点击查看代码
​```js
console.log('你好，VuePress！')
​```
:::
```



::: danger STOP
危险区域，禁止通行
:::

::: details 点击查看代码
```js
console.log('你好，VuePress！')
```
:::

## 代码块中的语法高亮

VuePress 使用了 [Prism](https://prismjs.com/)来为 markdown 中的代码块实现语法高亮。Prism 支持大量的编程语言，你需要做的只是在代码块的开始倒勾中附加一个有效的语言别名：

**输入**

```
​``` js
export default {
  name: 'MyComponent',
  // ...
}
​```
```

**输出**

```js
export default {
  name: 'MyComponent',
  // ...
}
```

在 Prism 的网站上查看 [合法的语言列表 ](https://prismjs.com/#languages-list)。



## 代码块中的行高亮

**输入**

```
​``` js {4}
export default {
  data () {
    return {
      msg: 'Highlighted!'
    }
  }
}
​```
```

**输出**

``` js {4}
export default {
  data () {
    return {
      msg: 'Highlighted!'
    }
  }
}
```

除了单行以外，你也可指定多行，行数区间，或是两者都指定。

- 行数区间: 例如 {5-8}, {3-10}, {10-17}
- 多个单行: 例如 {4,7,9}
- 行数区间与多个单行: 例如 {4,7-13,16,23-27,40}

``` js{1,4,6-7}
export default { // Highlighted
  data () {
    return {
      msg: `Highlighted!
      This line isn't highlighted,
      but this and the next 2 are.`,
      motd: 'VuePress is awesome',
      lorem: 'ipsum',
    }
  }
}
```

## 相关链接
- [VuePress官网对markdown扩展的介绍](https://www.vuepress.cn/guide/markdown.html)
- [Markdown 官方教程](https://markdown.com.cn/)
- [markdown-it支持的Emoji](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/full.json)