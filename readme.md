## 项目说明
使用VuePress应用vuepress-theme-reco主题搭建博客，相关博客介绍[使用VuePress应用vuepress-theme-reco主题搭建博客](https://blog.mbbbc.xyz/views/vuepress/vuepress-start.html)。**该master分支是使用vuepress-theme-reco主题美化后的(推荐)！**

## 项目演示
[https://blog.mbbbc.xyz](https://blog.mbbbc.xyz)。（个人进行了信息的替换，不过整体结构不变）

## 部署说明

> 前提条件 VuePress 需要 [Node.js](https://nodejs.org/en/)>= 8.6

0. 拉取代码到本地

1. 依赖安装
```shell
npm install
```

2. 本地运行
```shell
npm run dev
```
VuePress 会在 http://localhost:8080 (若8080端口被占用会生成其它的端口，观察控制台输出的地址即可)启动一个热重载的开发服务器

3. 打包到服务器，默认会打包到.vuepress/dist，把dist部署到服务器相应配置nginx来访问，当然也可以使用gitee/github提供的网页托管服务，需要的可以自行搜索。
```shell
npm run build
```